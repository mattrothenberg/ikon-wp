<?php

namespace App;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function shortTitle($id) {
        if ( get_field('short_title', $id)) {
            return get_field('short_title', $id);
        } else {
            return get_the_title($id);
        }
    }

    public static function currentPostIssue ($post) {
        $terms = get_the_terms( $post->ID, 'issue' );
        if ( !empty( $terms ) ){
            $term = array_shift( $terms );
            return $term->slug;
        }
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return single_cat_title( '', false );
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
}
