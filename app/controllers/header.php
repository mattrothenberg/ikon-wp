<?php

namespace Header;

use Sober\Controller\Controller;

class Header extends Controller
{
    public static function onAboutPage()
    {
      return is_page('about');
    }

    public function currentPostIssue () {
      $currentIssue = get_field('current_issue', 'option');
      $terms = get_the_terms( $post->ID, 'issue' );

      if (empty( $terms )) {
        return $currentIssue;
      }

      $term = array_shift( $terms );
      if ($term->slug != $currentIssue->slug) {
        return $term;
      } else {
        return $currentIssue;
      }
    }

    public static function previousLink () {
      $previous;
      $currentPostIssue = Header::currentPostIssue();

      get_adjacent_post(true, '', true, 'issue')
        ? $previous = get_previous_post()
        : $previous = get_posts(
          array(
            'posts_per_page' => 1,
            'order' => 'DESC',
            'tax_query' => array(
              array(
                'taxonomy' => 'issue',
                'field' => 'slug',
                'terms' => array($currentPostIssue->slug),
              )
            )
          )
        );

      if (is_array($previous)) {
        return array_pop($previous);
      } else {
        return $previous;
      }
    }

    public static function nextLink () {
      $next;
      $currentPostIssue = Header::currentPostIssue();

      get_adjacent_post(true, '', false, 'issue')
        ? $next = get_next_post()
        : $next = get_posts(
          array(
            'posts_per_page' => 1,
            'order' => 'ASC',
            'tax_query' => array(
              array(
                'taxonomy' => 'issue',
                'field' => 'slug',
                'terms' => array($currentPostIssue->slug),
              )
            )
          )
        );

      if (is_array($next)) {
        return array_pop($next);
      } else {
        return $next;
      }
    }

    public static function getPosts($count) {
      $args = array(
        'posts_per_page' => $count,
        'order' => 'ASC',
        'exclude' => get_the_ID(),
        'tax_query' => array(
          array(
            'taxonomy' => 'issue',
            'field' => 'slug',
            'terms' => array(get_field('current_issue', 'option')->slug),
          )
        )
      );

      return get_posts( $args );
    }
}
