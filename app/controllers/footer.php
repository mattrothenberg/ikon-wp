<?php

namespace Footer;

use Sober\Controller\Controller;

class Footer extends Controller
{
  public static function issueList() {
    return get_terms('issue');
  }
}