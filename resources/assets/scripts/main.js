// import external dependencies
import 'jquery';

// Import everything from autoload
import "./autoload/**/*"

import router from './util/Router';

// Load Events
jQuery(document).ready(() => router.loadEvents());
