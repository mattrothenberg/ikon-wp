import "flickity-fullscreen";
import Flickity from "flickity";

export default {
  init() {
    const gallery = document.querySelectorAll(".flickity-gallery");
    const videos = document.querySelectorAll("video");

    if (videos.length) {
      videos.forEach(video => {
        video.removeAttribute("width");
        video.removeAttribute("height");
      });
    }

    if (gallery.length) {
      new Flickity(".flickity-gallery", {
        lazyLoad: true,
        fullscreen: true,
        pageDots: false,
        autoPlay: true
      });
    }
  }
};
