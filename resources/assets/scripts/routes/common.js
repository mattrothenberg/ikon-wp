import "smoothstate";
import router from "../util/Router";

export default {
  init() {
    const pjaxContainer = $("#pjax");
    pjaxContainer.smoothState({
      blacklist: ".wp-admin *, .single-post .entry-content a",
      onStart: {
        render($container) {
          $container.addClass("slide-out");
        }
      },
      onReady: {
        render: function($container, $newContent) {
          var oldContentDOM = $container[0];
          var newContentDOM = $newContent[0];
          var oldIssue = oldContentDOM.querySelector(".current-issue span")
            .innerHTML;
          var newIssue = newContentDOM.querySelector(".current-issue span")
            .innerHTML;
          if (oldIssue !== newIssue) {
            $container.addClass("animate-category");
          }
          $container.html($newContent);
        }
      },
      onAfter($container) {
        router.loadEvents();
        $container.removeClass("slide-out");
        $container.removeClass("animate-category");
      }
    });
  }
};
