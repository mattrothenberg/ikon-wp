<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body>
    <div id="pjax">
      <div id="main" @php(body_class())>
        @php(do_action('get_header'))
        @include('partials.header')
        <div role="document">
          <main class="main max-w-md px-4 mx-auto">
            <div class="py-4 md:py-8">
              @yield('content')
            </div>
          </main>
          @if (App\display_sidebar())
            <aside class="sidebar">
              @include('partials.sidebar')
            </aside>
          @endif
        </div>
        @php(do_action('get_footer'))
        @include('partials.footer')
        @include('partials.issue-button')
        </div>
      </div>
    @php(wp_footer())
  </body>
</html>
