@extends('layouts.app')

@section('content')
  <div class="typeset">
    <div class="mb-4">
      @include('partials.page-header')
    </div>
    @while(have_posts()) @php(the_post())
      @include('partials.content-search')
    @endwhile
  </div>
@endsection
