<p class="byline author vcard">
  {{ __('by', 'sage') }}
  <?php $authors = get_field('author') ?>
  <span>
    {{ $authors[0]->post_title }}
  </span>
</p>