<article @php(post_class())>
  <header>
    <h1 class="entry-title delta"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h1>
    @if (get_post_type() === 'post')
      @include('partials/entry-meta')
    @endif
  </header>
</article>
