<div class="typeset">
  @php(the_content())
  @if (get_field('author'))
    <hr>
    <footer>
      @foreach (get_field('author') as $posts)
        <span>{!! $posts->post_content !!}</span>
      @endforeach
    </footer>
  @endif
</div>
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
