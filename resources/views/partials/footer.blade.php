<footer>
  <div class="max-w-md mx-auto px-4 py-4">
    <h1 class="font-sans uppercase tracking-wide text-sm">Issues</h1>
    <ul class="list-reset typeset">
      @foreach (Footer::issueList() as $issue)
        <li>
          <a class="pjax-link" href="{{ get_term_link($issue) }}">
            {{ $issue->name }}
          </a>
        </li>
      @endforeach
    </ul>
  </div>
</footer>
