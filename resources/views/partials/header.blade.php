<header class="max-w-md mx-auto px-4 text-center">
  <a class="logo inline-block mx-auto no-underline my-6" href="{{ get_home_url() }}">
    <img class="mx-auto block" src="@asset('images/logo.svg')">
  </a>
  <nav class="flex flex-col sm:flex-row text-center md:text-left justify-between sm:items-center py-2 text-left">
    @if ( Header::onAboutPage() )
      @foreach (Header::getPosts(3) as $post)
        <a href="{{get_permalink($post)}}">
          {{ App::shortTitle($post->ID) }}
        </a>
      @endforeach
    @elseif ( is_tax('issue') )
      <a href="{{ home_url('/about') }}">About</a>
      <span class="hidden sm:block spacer relative">
      </span>
    @elseif ( is_single() )
      <a href="{{ home_url('/about') }}">About</a>
      <a href="{{get_permalink(Header::nextLink())}}">
        {{ App::shortTitle( Header::nextLink()->ID ) }}
      </a>
      <a href="{{get_permalink(Header::previousLink())}}">
          {{ App::shortTitle( Header::previousLink()->ID ) }}
      </a>
    @endif
  </nav>
</header>