<div class="current-issue hidden md:flex">
  <span class="text-sm">
    {{ Header::currentPostIssue()->name }}
  </span>
</div>