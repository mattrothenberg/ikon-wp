<article class="typeset" @php(post_class())>
  <header>
    <h1 class="entry-title">{{ get_the_title() }}</h1>
    @include('partials/entry-meta')
  </header>
  <div class="entry-content">
    @php(the_content())
  </div>
  <hr>
  <footer>
    @foreach (get_field('author') as $posts)
      <div class="mt-4 author">{!! $posts->post_content !!}</div>
    @endforeach
  </footer>
</article>
