<div class="issue-nav fixed pin bg-black h-full w-full">
  <button id="js-nav-close">Close</button>
  <div class="px-4 max-w-3xl mx-auto w-full flex h-full items-center justify-start">
    <div>
      @foreach (Footer::issueList() as $issue)
      <div class="mb-8">
        <a class="pjax-link text-white text-4xl block" href="{{ get_term_link($issue) }}">
          {{ $issue->name }}
        </a>
      </div>
      @endforeach
    </div>
  </div>
</div>